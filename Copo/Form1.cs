﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Copo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonNovoCopo_Click(object sender, EventArgs e)
        {
            Copo c = new Copo("Cerveja",0.35);
            labelStatus.Text = c.ToString() + Environment.NewLine;

            Copo b = new Copo(c);
            labelStatus.Text += b.ToString();
        }

        private void labelStatus_Click(object sender, EventArgs e)
        {

        }
    }
}
