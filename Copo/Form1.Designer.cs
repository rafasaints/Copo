﻿namespace Copo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonNovoCopo = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonNovoCopo
            // 
            this.buttonNovoCopo.Location = new System.Drawing.Point(129, 45);
            this.buttonNovoCopo.Name = "buttonNovoCopo";
            this.buttonNovoCopo.Size = new System.Drawing.Size(90, 52);
            this.buttonNovoCopo.TabIndex = 0;
            this.buttonNovoCopo.Text = "Novo Copo";
            this.buttonNovoCopo.UseVisualStyleBackColor = true;
            this.buttonNovoCopo.Click += new System.EventHandler(this.buttonNovoCopo_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(12, 148);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(46, 17);
            this.labelStatus.TabIndex = 2;
            this.labelStatus.Text = "label2";
            this.labelStatus.Click += new System.EventHandler(this.labelStatus_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 262);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.buttonNovoCopo);
            this.Name = "Form1";
            this.Text = "Projecto Copo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonNovoCopo;
        private System.Windows.Forms.Label labelStatus;
    }
}

